import numpy as np
import matplotlib.pyplot as plt
from random import random as rnd
from time import sleep

fig, ax = plt.subplots()
eps = float(input(" epsilon > "))
steps = int(input(" steps > "))

def mod2pi(s):
    if s > 2*np.pi:
        s -= 2*np.pi
    elif s < 0:
        s += 2*np.pi
    else:
        return s
    return mod2pi(s)

def standard_map(s, v):
    v = eps*np.sin(s) + v
    s += v
    s = mod2pi(s)
    return s, v

def plot(x, y):
    x_data = np.array([0.0 for i in range(steps + 1)])
    y_data = np.array([0.0 for i in range(steps + 1)])
    x_data[0], y_data[0] = x, y
    for i in range(steps):
        if not i % 1000:
            print(i)
        x_data[i+1], y_data[i+1] = standard_map(x_data[i], y_data[i])
    ax.plot(x_data, y_data, 'b.', markersize=1)
    ax.set_title("epsilon = {}, steps = {}, orbit of ({}, {})".format(eps, steps, x, y))

"""
dy = 0.02
for i in range(-25, 25):
    plot(0, i*dy)
for i in range(0, 100):
    plot(2*np.pi*i/100, 0)
plt.xlim(0, 2*np.pi)
# plt.ylim(-0.1, 0.1)
fig.savefig("./images/simple-pendulum-e{}-steps{}.pdf".format(eps, steps))
"""

def single_orbit(x, y):
    plot(x, y)
    plt.xlim(0, 2*np.pi)
    fig.savefig("./single_orbits/simple-pendulum-e{}-steps{}-orbit({},{}).pdf".format(eps,steps,x,y))
    plt.show()

x = float(input(" x > "))
y = float(input(" y > "))
single_orbit(x, y)
